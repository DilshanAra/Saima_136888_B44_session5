<?php
//boolean example started here

//if(expression==true){do this}

$decision = true;
if($decision)
    echo "the dicision is true";

$decision = false;
if($decision)
    echo "the dicision is false";

//boolean example ended here

//Integer example started here

echo "<br>";
$value = 100;
echo $value . "<br>";

//Integer example ended here

//Float example started here

$value = 86.12;
echo $value . "<br>";

//Float example ended here


//Integer & Float example started here

$value = 100;
$value1 = 86.12;
echo $value . " & " . $value1;

//Integer & Float example ended here

//String example started here

echo "<br>";

$var = 100;
//use '' to print exactly same
$value1 = 'single string $var <br>';
//use " "to print as command
$value2 = "My string $var <br>";
echo $value1 . $value2;

//String example ended here

//String(Heredoc & nowdoc) example started here

echo "<br>";

//Heredoc example(print as cmmand)

$heredocString = <<<BITM
    line 1 $var <br>
    line 2 $var <br>
    line 3 $var <br>

BITM;

//Nowdoc example(print exactly the same)

$nowdocString = <<<'BITM'
    line 1 $var <br>
    line 2 $var <br>
    line 3 $var <br>

BITM;

echo $heredocString . "<br> <br>". $nowdocString . "<br>";

//String(Heredoc & nowdoc) example ended here

// StringArray example started here

$arr = array(1,2,3,4,5,6,7,8,9,10);
print_r($arr);
echo "<br><br>";

$arr = array("BMW","TOYOTA","NISSAN","FORD","FERARI","MARUTI");
print_r($arr);
echo "<br><br>";
var_dump($arr);

echo "<br><br>";
$ageArray= array("Arif"=>30, "Moynar Ma"=>45, "Rahim"=>25);
print_r($ageArray);
echo "<br><br>";
echo "the age of rahim is" . $ageArray["Rahim"];

// StringArray example ended here

// Super Golobals Variable example started here

echo "<br><br>";
$x = 75;
function do_something()
{
    $x = 25;
    echo "local x = $x";
    echo "<br><br>";
    echo "global x = " . $GLOBALS['x'];
}
do_something();

// Super Golobals Variable example ended here

// Server example started here

echo "<br><br>";
echo $_SERVER['PHP_SELF'] . "<br>";
echo $_SERVER['SERVER_NAME'] . "<br>";
echo $_SERVER['HTTP_REFERER'] . "<br>";
echo $_SERVER['SCRIPT_FILENAME'] . "<br>";
echo $_COOKIE['SCRIPT_COOKIE'] . "<br>";

// Server example ended here

































